package Controlador;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.pame.libros.utils.Gestor;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import utils.Utils;
import utils.modelos.EditorialModelo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static utils.PantallasEmergentes.mensajeMuestra;
import static utils.PantallasEmergentes.muestraExcepcion;

public class SelectorEditorial {

    @FXML
    private JFXTextField selectorEditorial_txtFiltro;

    @FXML
    private TableView<EditorialModelo> selectorEditorial_tableView;

    @FXML
    private TableColumn<EditorialModelo, String> selectorEditorial_tableColumnNombreEditoriales;

    @FXML
    private TableColumn<EditorialModelo, CheckBox> selectorEditorial_tableColumnCheckBoxes;

    @FXML
    private JFXButton selectorEditorial_btnAddEditorial, selectorEditorial_btnAceptar, selectorEditorial_btnSalir;

    @FXML
    private JFXTextField selectorEditorial_txtAddEditorial;

    @FXML
    private Label selectorEditorial_labelFiltrar;

    @FXML
    private VBox selectorEditorial_vBox;


    private final ObservableList<EditorialModelo> masterData = FXCollections.observableArrayList();
    private final ObjectProperty<Predicate<EditorialModelo>> nameFilter = new SimpleObjectProperty<>();
    public static List<EditorialModelo> elementosSeleccionados = new ArrayList<>();

    @FXML
    private void initialize() {
        selectorEditorial_vBox.getStyleClass().add("vbox");
        obtenerEditoriales();
        creaCheckBoxesColumn();
        creaFiltrosPorNombre();
    }

    private void creaFiltrosPorNombre() {
        selectorEditorial_tableColumnNombreEditoriales.setCellValueFactory(cellData -> cellData.getValue().nombreEditorialProperty());

        nameFilter.bind(Bindings.createObjectBinding(() ->
                        editorial -> editorial.getNombreEditorial().toLowerCase().contains(selectorEditorial_txtFiltro.getText().toLowerCase()),
                selectorEditorial_txtFiltro.textProperty()));

        refreshFilteredList();
    }

    private void creaCheckBoxesColumn() {
        selectorEditorial_tableColumnCheckBoxes.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<EditorialModelo, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(
                    TableColumn.CellDataFeatures<EditorialModelo, CheckBox> arg0) {
                EditorialModelo user = arg0.getValue();

                CheckBox checkBox = new CheckBox();
                checkBox.selectedProperty().setValue(user.isSeleccionado());
                checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                    public void changed(ObservableValue<? extends Boolean> ov,
                                        Boolean old_val, Boolean new_val) {
                        user.setSeleccionado(new_val);
                    }
                });
                return new SimpleObjectProperty<>(checkBox);
            }
        });
    }

    private void obtenerEditoriales() {

        ArrayList<String> editoriales = Gestor.obtenerTodasLasEditorialesEnJson();
        ArrayList<EditorialModelo> t = null;
        try {
            t = Utils.transformaDeJsonAObjetoEditorial(editoriales);
            masterData.addAll(t);
        } catch (IOException e) {
            muestraExcepcion("Error al obtener las editoriales de la Base de datos", e.getMessage());
            e.printStackTrace();
        }

    }

    @FXML
    private void selectorEditorial_btnAddEditorialOnAction() {

        String editorial = selectorEditorial_txtAddEditorial.getText();


        try {
            if (Gestor.existeEditorial(editorial))
                mensajeMuestra("La editorial con nombre \" " + editorial + "\" ya existe en la Base de datos", Alert.AlertType.WARNING);
            else {
                try {
                    masterData.add(utils.Utils.transformaDeJsonAObjetoEditorial(Gestor.aniadeEditorial(editorial)));
                    refreshFilteredList();
                    mensajeMuestra("Editorial añadida con éxito a la Base de datos", Alert.AlertType.INFORMATION);

                } catch (Exception e) {
                    muestraExcepcion("Error al intentar añadir la editorial a la Base de datos", e.getMessage());

                }
            }
        } catch (Exception e) {
            muestraExcepcion("Error al intentar añadir la editorial a la Base de datos", e.getMessage());
        }
    }

    private void refreshFilteredList() {
        FilteredList<EditorialModelo> filteredItems = new FilteredList<>(FXCollections.observableList(masterData));

        selectorEditorial_tableView.setItems(filteredItems);

        filteredItems.predicateProperty().bind(Bindings.createObjectBinding(
                () -> nameFilter.get(),
                nameFilter));
    }

    @FXML
    private void selectorEditorial_btnAceptarOnAction() {
        elementosSeleccionados = masterData.stream().filter(editorialModelo -> editorialModelo.isSeleccionado()).collect(Collectors.toList());
        Stage stage = (Stage) selectorEditorial_btnAceptar.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void selectorEditorial_btnSalirOnAction() {
        elementosSeleccionados.clear();
        Stage stage = (Stage) selectorEditorial_btnSalir.getScene().getWindow();
        stage.close();
    }

}
