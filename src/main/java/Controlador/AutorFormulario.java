package Controlador;

import com.pame.libros.utils.Gestor;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import static utils.PantallasEmergentes.mensajeMuestra;

public class AutorFormulario {

    @FXML
    private TextField autor_txtNombre;

    @FXML
    private TextField autor_txtApellido;

    @FXML
    private TextField autor_txtWeb;

    @FXML
    private Button autor_btnGuardar;

    @FXML
    private Button autor_btnSalir;

    @FXML
    private void autor_btnSalirOnAction() {
        Stage stage = (Stage) autor_btnSalir.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void autor_btnGuardarOnAction() {
        String nombreAutor = autor_txtNombre.getText().trim();
        if (!nombreAutor.isEmpty()) {

            if (Gestor.aniadeAutor(nombreAutor, autor_txtApellido.getText(), autor_txtWeb.getText()).isEmpty())
                mensajeMuestra("No se ha podido añadir el Autor", Alert.AlertType.WARNING);
            else
                mensajeMuestra("Autor añadido con éxito", Alert.AlertType.INFORMATION);
        } else
            mensajeMuestra("El campo nombre es obligatorio", Alert.AlertType.INFORMATION);


    }


}
