package Controlador;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.pame.libros.utils.Gestor;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import static utils.PantallasEmergentes.mensajeMuestra;
import static utils.PantallasEmergentes.muestraExcepcion;

public class FichaEditorial {

    @FXML
    JFXButton edit_btnGuardar;
    @FXML
    JFXButton edit_btnSalir;
    @FXML
    private JFXTextField fichaEdit_nombreEditorial;

    @FXML
    private void edit_btnGuardarOnAction() {
        String editorial = fichaEdit_nombreEditorial.getText();

        if (Gestor.existeEditorial(editorial)) {
            mensajeMuestra("La editorial con nombre \" " + editorial + "\" ya existe en la Base de datos", Alert.AlertType.WARNING);
        } else {
            try {
                Gestor.aniadeEditorial(editorial);
                mensajeMuestra("Editorial añadida con éxito a la Base de datos", Alert.AlertType.INFORMATION);
            } catch (Exception e) {
                muestraExcepcion("Error al intentar añadir la editorial a la Base de datos", e.getMessage());
            }
        }
    }

    @FXML
    private void edit_btnSalirOnAction() {
        Stage stage = (Stage) edit_btnSalir.getScene().getWindow();
        stage.close();
    }
}
