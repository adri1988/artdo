package Controlador;


import com.pame.libros.utils.Gestor;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.net.URL;


public class Principal extends Application {

    @FXML
    private MenuItem  principal_cerrar;
    private Window ventana_principal;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Gestor.Init();
        Parent root = FXMLLoader.load(getClass().getResource("/Pantallas/Principal.fxml"));
        primaryStage.getIcons().add(new Image(Principal.class.getResourceAsStream("/Imagenes/LogoApp.png")));
        primaryStage.setTitle("ArtDo!");
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        ventana_principal = primaryStage.getScene().getWindow();
        primaryStage.show();
    }

    @FXML
    private void principal_llamaAAniadirObra() {
        try {
            Parent rootObra = FXMLLoader.load(getClass().getResource("/Pantallas/Autor.fxml"));
            Stage stageDeObra = new Stage();
            stageDeObra.getIcons().add(new Image(Principal.class.getResourceAsStream("/Imagenes/LogoApp.png")));
            stageDeObra.setTitle("ArtDo!");

            Scene scene = new Scene(rootObra, 575, 346, Color.WHITESMOKE);
            stageDeObra.setScene(scene);
            stageDeObra.initModality(Modality.APPLICATION_MODAL);
            stageDeObra.initOwner(ventana_principal);
            stageDeObra.show();
        } catch (Exception p) {
            p.printStackTrace();
        }
    }

    @FXML
    private void principal_llamaAniadir_libro() {
        try {
            creaYLlamaVentana(getClass().getResource("/Pantallas/Libro.fxml"), ventana_principal);
        } catch (Exception p) {
            p.printStackTrace();
        }
    }

    @FXML
    private void principal_llamaAniadir_edit() {
        try {
            creaYLlamaVentana(getClass().getResource("/Pantallas/FichaEditorial.fxml"), ventana_principal);
        } catch (Exception p) {
            p.printStackTrace();
        }
    }

    @FXML
    private void principal_ver_librosOnAction() {

        try {
            creaYLlamaVentana(getClass().getResource("/Pantallas/SelectorLibros.fxml"), ventana_principal);
        } catch (Exception p) {
            p.printStackTrace();
        }

    }

    private void creaYLlamaVentana(URL resource, Window ventana_principal) throws java.io.IOException {
        Parent rootLibro = FXMLLoader.load(resource);
        Stage stageDeObra = new Stage();
        stageDeObra.getIcons().add(new Image(Principal.class.getResourceAsStream("/Imagenes/LogoApp.png")));
        stageDeObra.setTitle("ArtDo!");
        Scene scene = new Scene(rootLibro);
        stageDeObra.setScene(scene);
        stageDeObra.initModality(Modality.APPLICATION_MODAL);
        stageDeObra.initOwner(ventana_principal);
        stageDeObra.show();
    }


    @FXML
    private void principal_cerrar() {
        /*
        * Aquí debo cerrar conexiones y liberar recursos
        * */
        System.exit(0);
    }


}
