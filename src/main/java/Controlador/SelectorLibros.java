package Controlador;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.pame.libros.utils.Gestor;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import utils.Utils;
import utils.modelos.LibroModelo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.function.Predicate;

public class SelectorLibros {

    @FXML
    private JFXTextField selectorLibro_txtTitulo;

    @FXML
    private JFXTextField selectorLibro_txtEditorial;

    @FXML
    private JFXTextField selectorLibro_txtIdioma;

    @FXML
    private JFXTextField selectorLibro_txtTematica;

    @FXML
    TableColumn<LibroModelo, CheckBox> selectorLibro_tableColumCheckBoxes;

    @FXML
    private JFXTextField selectorLibro_txtISBN;

    @FXML
    private JFXTextField selectorLibro_txtAutores;

    @FXML
    public TableView<LibroModelo> selectorLibro_tableLibro;

    @FXML
    private TableColumn<LibroModelo, String> selectorLibro_columnaTitulo;
    @FXML
    private JFXComboBox<String> selectorLibro_comboSoporte;
    @FXML
    private TableColumn<LibroModelo, ImageView> selectorLibro_columnaImagen;

    @FXML
    private TableColumn<LibroModelo, String> selectorLibro_columnaIdioma;

    @FXML
    private TableColumn<LibroModelo, String> selectorLibro_columnaFecha;

    @FXML
    private TableColumn<LibroModelo, String> selectorLibro_columnaTematica;

    @FXML
    private TableColumn<LibroModelo, String> selectorLibro_columnaSoporte;

    @FXML
    private TableColumn<LibroModelo, String> selectorLibro_columnaISBN;
    @FXML
    private TableColumn<LibroModelo, String> selectorLibro_columnaEditorial, selectorLibro_columnaAutores;

    @FXML
    private TableColumn<LibroModelo, HBox> selectorLibro_columnaDelModif;




    private ObservableList<LibroModelo> masterData = FXCollections.observableArrayList();

    private final ObjectProperty<Predicate<LibroModelo>> tituloFilter = new SimpleObjectProperty<>();
    private final ObjectProperty<Predicate<LibroModelo>> editorialFilter = new SimpleObjectProperty<>();


    @FXML
    public void initialize() {


        masterData = FXCollections.observableArrayList();
        ArrayList<String> librosEnJson = Gestor.obtenerTodosLosLibrosEnJson();
        try {
            masterData.addAll(Utils.transformaDeJsonAObjetoLibro(librosEnJson));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Utils.populaComboBoxSoporte(selectorLibro_comboSoporte);
        creaFiltrosTituloEditorial();
    }

    @FXML
    private void actualizarArbol(KeyEvent event) {

        if (event.getCode().getName().equals("F11")) {
            initialize();
        }


    }


    private void creaFiltrosTituloEditorial() {

        selectorLibro_columnaTitulo.setCellValueFactory(cellData -> cellData.getValue().tituloProperty());
        selectorLibro_columnaEditorial.setCellValueFactory(cellData -> cellData.getValue().editorialProperty());
        selectorLibro_columnaImagen.setCellValueFactory(cellData -> cellData.getValue().getImageView());
        selectorLibro_columnaAutores.setCellValueFactory(cellData -> cellData.getValue().autoresProperty());
        selectorLibro_columnaIdioma.setCellValueFactory(cellData -> cellData.getValue().idiomaProperty());
        selectorLibro_columnaFecha.setCellValueFactory(cellData -> cellData.getValue().anioPublicacionProperty());
        selectorLibro_columnaTematica.setCellValueFactory(cellData -> cellData.getValue().tematicaProperty());
        selectorLibro_columnaISBN.setCellValueFactory(cellData -> cellData.getValue().ISBNProperty());
        selectorLibro_columnaSoporte.setCellValueFactory(cellData -> cellData.getValue().soporteProperty());
        selectorLibro_columnaDelModif.setCellValueFactory(cellData -> cellData.getValue().botoneraModifyDelete(this));

        tituloFilter.bind(Bindings.createObjectBinding(() ->
                        libro -> libro.getTitulo().toLowerCase().contains(selectorLibro_txtTitulo.getText().toLowerCase())
                , selectorLibro_txtTitulo.textProperty()));

        editorialFilter.bind(Bindings.createObjectBinding(() ->
                libro -> libro.getEditorial().toLowerCase().contains(selectorLibro_txtEditorial.getText().toLowerCase()), selectorLibro_txtEditorial.textProperty()));

        refreshFilteredList();


    }

    private void refreshFilteredList() {
        FilteredList<LibroModelo> filteredItems = new FilteredList<>(FXCollections.observableList(masterData));
        selectorLibro_tableLibro.setItems(filteredItems);
        filteredItems.predicateProperty().bind(Bindings.createObjectBinding(
                () -> tituloFilter.get().and(editorialFilter.get()),
                tituloFilter, editorialFilter));
    }


}
