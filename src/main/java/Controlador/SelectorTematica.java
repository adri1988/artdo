package Controlador;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.stage.Stage;
import org.controlsfx.control.CheckTreeView;
import utils.tematicaUtils.Hoja;
import utils.tematicaUtils.Nodo;
import utils.tematicaUtils.Raiz;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SelectorTematica {
    @FXML
    private static Raiz raiz = null;
    @FXML
    private CheckTreeView<String> selectorTematica_checkTreeView;
    @FXML
    private JFXButton selectorTematica_btnSalir, selectorTematica_btnAceptar;

    public static final List<String> listaDeTematicas = new ArrayList<>();

    @FXML
    private void initialize() {
        if (raiz == null) {
            raiz = new Raiz();
            raiz = creaObjetoRaizDeXML(raiz, "/tematicas/tematicas.xml");
        }
        CheckBoxTreeItem<String> raizDeArbol = mapeaObjeto(raiz);

        selectorTematica_checkTreeView.setRoot(raizDeArbol);
        selectorTematica_checkTreeView.setVisible(true);
    }


    @FXML
    private void selectorTematica_btnSalirOnAction() {

        listaDeTematicas.clear();
        Stage stage = (Stage) selectorTematica_btnSalir.getScene().getWindow();
        stage.close();

    }

    @FXML
    private void selectorTematica_btnAceptarOnAction() {

        selectorTematica_checkTreeView.getCheckModel().getCheckedItems().forEach(e -> {
            System.out.println(e.getValue());
            listaDeTematicas.add(e.getValue());
        });
        Stage stage = (Stage) selectorTematica_btnAceptar.getScene().getWindow();
        stage.close();

    }


    private Raiz creaObjetoRaizDeXML(Raiz raiz, String ficheroXML) {
        try {

            File tematicaXML = new File(SelectorTematica.class.getResource(ficheroXML).getFile());
            raiz = new XmlMapper().readValue(tematicaXML, Raiz.class);
            System.out.println(raiz);
        } catch (IOException e) {
            //TODO : ventana emergente de que no se ha podido mostrar el árbol de temáticas.
            e.printStackTrace();
        }
        return raiz;
    }

    private CheckBoxTreeItem<String> mapeaObjeto(Raiz raiz) {


        CheckBoxTreeItem<String> root = new CheckBoxTreeItem<>("raiz");
        root.setExpanded(true);

        for (int i = 0; i < raiz.getNodo().length; i++) {
            creaNodoA(root, raiz.getNodo()[i]);
        }

        return root;

    }

    private void creaNodoA(CheckBoxTreeItem<String> treeItem, Nodo nodo) {
        CheckBoxTreeItem<String> item = new CheckBoxTreeItem<>(nodo.getNombre());
        treeItem.getChildren().add(item);


        if (nodo.getNodo() != null) {
            for (int i = 0; i < nodo.getNodo().length; i++) {
                creaNodoA(item, nodo.getNodo()[i]);
            }
        }


        if (nodo.getHoja() != null) {
            for (int i = 0; i < nodo.getHoja().length; i++) {
                creaHojaA(item, nodo.getHoja()[i]);
            }
        }
    }

    private void creaHojaA(CheckBoxTreeItem<String> item, Hoja hoja) {

        CheckBoxTreeItem<String> itemHoja = new CheckBoxTreeItem<>(hoja.getNombre());
        item.getChildren().add(itemHoja);

    }

}
