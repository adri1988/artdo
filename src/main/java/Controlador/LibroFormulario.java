package Controlador;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.NumberValidator;
import com.jfoenix.validation.RequiredFieldValidator;
import com.pame.libros.utils.Gestor;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.TilePane;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.controlsfx.control.CheckComboBox;
import utils.Utils;
import utils.modelos.AutorModelo;
import utils.modelos.EditorialModelo;
import utils.modelos.LibroModelo;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

import static utils.Utils.obtenerImagenEnBase64;

public class LibroFormulario {
    @FXML
    private JFXButton libro_btnGuardar;

    @FXML
    private JFXButton libro_btnSalir;

    private static LibroModelo libroModeloActualizado = null;


    @FXML
    private TextArea libro_txtNotas;

    @FXML
    private RequiredFieldValidator libro_requiredFieldValidator;

    @FXML
    private HTMLEditor libro_htmlEditor;

    @FXML
    private NumberValidator libro_numberValidator, libro_numPaginasValidator;

    @FXML
    private JFXComboBox<String> libro_ComboSoporteCombo;

    @FXML
    private JFXButton libro_AniadirImagen;

    @FXML
    private TilePane libro_TilePane;


    private ContextMenu menuEliminarImagen;
    private ImageView imagenSeleccionada = null;
    private ArrayList<AutorModelo> autoresSeleccionados = null;
    private ArrayList<EditorialModelo> editorialesSeleccionadas = null;
    private static String datos = null;
    @FXML
    private JFXTextField libro_txtEditorial,
            libro_txtTraductor,
            libro_txtCiudad,
            libro_txtPais,
            libro_txtIdioma,
            libro_txtNumPaginas,
            libro_txtTematica,
            libro_txtAnio,
            libro_txtISBN,
            libro_txtUbicacionFisica,
            libro_txtTitulo,
            libro_txtAutores;
    private Long idLibro = null;

    @FXML
    private JFXTextField libro_txtNumEjemplares;

    public static void setInitDatos(String datosDeLibroEnJson) {

        datos = datosDeLibroEnJson;

    }

    public static LibroModelo getLibroModeloActualizado() {
        return libroModeloActualizado;
    }

    @FXML
    public void initialize() {
        libroModeloActualizado = null;
        autoresSeleccionados = new ArrayList<>();
        editorialesSeleccionadas = new ArrayList<>();

        libro_txtEditorial.setEditable(false);
        libro_txtAutores.setEditable(false);


        initComboBoxSoporte();
        initTituloValidator();
        initNumPaginasValidator();
        initMenuContextualEliminarImagen();

        initNumEjemplaresValidador();

        if (datos != null)
            initFieldsDeVentanaConDatos();
        datos = null;


    }

    private void initFieldsDeVentanaConDatos() {

        try {
            Map<String, Object> datosClaveValor = Utils.obtenerMapaDeString(datos);
            System.out.println(datosClaveValor);
            idLibro = Long.valueOf((Integer) datosClaveValor.get("idLibros"));
            libro_txtTitulo.setText((String) datosClaveValor.get("titulo"));
            libro_txtTraductor.setText((String) datosClaveValor.get("traductor"));
            libro_txtCiudad.setText((String) datosClaveValor.get("ciudad_publicacion"));
            libro_txtPais.setText((String) datosClaveValor.get("pais_publicacion"));

            libro_txtAutores.setText(obtenerAutores((ArrayList<Map<String, Object>>) datosClaveValor.get("autores")));
            libro_txtEditorial.setText(obtenerEditoriales((ArrayList<Map<String, Object>>) datosClaveValor.get("editoriales")));

            Integer anio_publicacion = (Integer) datosClaveValor.get("anio_de_publicacion");
            if (anio_publicacion != null)
                libro_txtAnio.setText(anio_publicacion.toString());


            libro_txtISBN.setText((String) datosClaveValor.get("isbn"));
            libro_txtIdioma.setText((String) datosClaveValor.get("idioma"));
            libro_txtTematica.setText((String) datosClaveValor.get("tematica"));

            String soporte = (String) datosClaveValor.get("soporte");
            if (soporte != null)
                libro_ComboSoporteCombo.getSelectionModel().select(soporte);

            Integer numPaginas = (Integer) datosClaveValor.get("numero_paginas");
            if (numPaginas != null)
                libro_txtNumPaginas.setText(numPaginas.toString());


            Integer numEjemplares = (Integer) datosClaveValor.get("numero_ejemplares");
            if (numEjemplares != null)
                libro_txtNumEjemplares.setText(numEjemplares.toString());


            libro_txtUbicacionFisica.setText((String) datosClaveValor.get("ubicacionFisica"));
            libro_htmlEditor.setHtmlText((String) datosClaveValor.get("observaciones_sinopsis"));
            libro_txtNotas.setText((String) datosClaveValor.get("notas"));


            insertarImagenes((ArrayList<Map<String, String>>) datosClaveValor.get("imagenes"));


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void insertarImagenes(ArrayList<Map<String, String>> imagenes) throws IOException {

        for (Map<String, String> mapaImagenes : imagenes) {
            ImageView i = Utils.obtenerImagen(mapaImagenes.get("imagen_libro"));
            libro_TilePane.getChildren().add(i);
        }

    }

    private String obtenerEditoriales(ArrayList<Map<String, Object>> editoriales) {

        String editorialesResultado = "";

        for (Map<String, Object> mapaEditorial : editoriales) {
            editorialesResultado = editorialesResultado + mapaEditorial.get("nombreEditorial") + " ,";
        }

        return editorialesResultado;

    }

    private String obtenerAutores(ArrayList<Map<String, Object>> autores) {

        String autoresResultado = "";

        for (Map<String, Object> mapaAutor : autores) {
            autoresResultado = autoresResultado + mapaAutor.get("nombre") + " " + mapaAutor.get("apellidos") + " ,";
        }

        return autoresResultado;

    }

    private void initComboBoxSoporte() {

        Utils.populaComboBoxSoporte(libro_ComboSoporteCombo);

    }


    @FXML
    private void libro_AniadirEditoriales() {

        try {
            llamaAVentanaSelectora(getClass().getResource("/Pantallas/SelectorEditorial.fxml"));
            String editoriales = "";
            editorialesSeleccionadas = (ArrayList<EditorialModelo>) SelectorEditorial.elementosSeleccionados;
            for (EditorialModelo editorialModelo : SelectorEditorial.elementosSeleccionados) {
                editoriales += editorialModelo.getNombreEditorial() + ", ";
            }
            libro_txtEditorial.setText(editoriales);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void libro_AniadirAutores() {

        try {
            llamaAVentanaSelectora(getClass().getResource("/Pantallas/SelectorAutores.fxml"));
            String autores = "";
            autoresSeleccionados = (ArrayList<AutorModelo>) SelectorAutores.elementosSeleccionados;
            for (AutorModelo autorModelo : SelectorAutores.elementosSeleccionados) {
                autores += autorModelo.getNombre() + " " + autorModelo.getApellidos() + ",";
            }
            libro_txtAutores.setText(autores);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void libro_btnAddTematicaOnAction() {

        try {
            llamaAVentanaSelectora(getClass().getResource("/Pantallas/SelectorTematica.fxml"));
            String tematicas = "";
            for (String tematica : SelectorTematica.listaDeTematicas) {
                tematicas += tematica + ", ";
            }
            libro_txtTematica.setText(tematicas);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void llamaAVentanaSelectora(URL resource) throws IOException {
        Parent root = FXMLLoader.load(resource);
        Stage primaryStage = new Stage();
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/Imagenes/LogoApp.png")));
        primaryStage.setTitle("ArtDo!");
        Scene scene = new Scene(root);
        scene.getStylesheets().add("estilo/estilo.css");
        primaryStage.setScene(scene);
        Window ventana_principal = primaryStage.getScene().getWindow();
        primaryStage.showAndWait();
    }

    private void initMenuContextualEliminarImagen() {
        menuEliminarImagen = new ContextMenu();
        final MenuItem item1 = new MenuItem("Eliminar");
        item1.setOnAction(e -> libro_TilePane.getChildren().remove(imagenSeleccionada));
        menuEliminarImagen.getItems().add(item1);
    }


    @FXML
    private void libro_AniadirImagenOnAction() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Añadir Imagen");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png"),
                new FileChooser.ExtensionFilter("All Images", "*.*")
        );
        fileChooser.showOpenMultipleDialog(libro_AniadirImagen.getScene().getWindow()).forEach(imagen -> {
            try {
                insertaImagenEnTilePane(imagen);
            } catch (MalformedURLException e) {
                gestionaExcepcion("No se ha podido añadir la imagen " + imagen.getName(), new ScrollPane(new TextArea(e.getMessage())));
            }
        });


    }

    private void gestionaExcepcion(String headerText, ScrollPane content) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(headerText);
        alert.getDialogPane().setExpandableContent(content);
        alert.showAndWait();
    }

    private void insertaImagenEnTilePane(File imagen) throws MalformedURLException {
        ImageView imagenAAniadir = new ImageView(new Image(imagen.toURI().toURL().toString(), 150, 150, true, true));
        imagenAAniadir.setOnContextMenuRequested(e -> {
            imagenSeleccionada = imagenAAniadir;
            menuEliminarImagen.show(imagenAAniadir, e.getScreenX(), e.getScreenY());
        });
        libro_TilePane.getChildren().add(imagenAAniadir);
    }

    private void initNumPaginasValidator() {
        libro_txtNumPaginas.getValidators().add(libro_numberValidator);
        libro_numberValidator.setMessage("Debe ser un número entero");
        libro_txtNumPaginas.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue)
                libro_txtNumPaginas.resetValidation();
        });
    }

    private void initNumEjemplaresValidador() {
        libro_txtNumEjemplares.getValidators().add(libro_numPaginasValidator);
        libro_numPaginasValidator.setMessage("Debe ser un número entero");
        libro_txtNumEjemplares.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue)
                libro_txtNumEjemplares.resetValidation();
        });
    }

    private void initTituloValidator() {

        libro_requiredFieldValidator.setMessage("El Título no puede estar en blanco");
        libro_txtTitulo.getValidators().add(libro_requiredFieldValidator);
        libro_txtTitulo.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue)
                libro_txtTitulo.resetValidation();
        });
    }


    @FXML
    private void libro_btnGuardarOnAction() {

        String libroTitulo = libro_txtTitulo.getText().trim();

        if (tituloEsValido() && numPaginasEsValido() && numEjemplaresEsValido())
            guardaLibro(libroTitulo);


    }

    private boolean numEjemplaresEsValido() {

        if (!libro_txtNumEjemplares.getText().isEmpty()) {

            libro_txtNumEjemplares.validate();
            if (libro_numPaginasValidator.getHasErrors())
                return false;
        }
        return true;
    }

    private void guardaLibro(String libroTitulo) {

        String paginas = libro_txtNumPaginas.getText();
        if (paginas.isEmpty()) {
            paginas = null;
        }
        String numEjemplares = libro_txtNumEjemplares.getText().trim();
        if (numEjemplares.isEmpty())
            numEjemplares = null;

        String anio = libro_txtAnio.getText().trim();
        if (anio.isEmpty())
            anio = null;




        ArrayList<String> imagenes = obtenerListaDeImagenesEnBase64();
        CheckComboBox<AutorModelo> libro_combosAutrores = null;

        new utils.Utils().TransformaAJson(autoresSeleccionados);


        String resultado = Gestor.aniadeLibro(idLibro, libroTitulo, new utils.Utils().TransformaAJson(autoresSeleccionados),
                libro_txtTraductor.getText().trim(),
                new utils.Utils().TransformaAJson(editorialesSeleccionadas),
                libro_txtCiudad.getText().trim(),
                libro_txtPais.getText().trim(),
                anio,
                libro_txtISBN.getText(),
                libro_txtIdioma.getText().trim(),
                libro_txtTematica.getText().trim(),
                libro_ComboSoporteCombo.getSelectionModel().getSelectedItem(),
                paginas,
                numEjemplares,
                libro_txtUbicacionFisica.getText().trim(),
                imagenes,
                libro_htmlEditor.getHtmlText(),
                libro_txtNotas.getText());


        ArrayList<String> libroEnJson = new ArrayList<>();
        libroEnJson.add(resultado);

        try {
            libroModeloActualizado = Utils.transformaDeJsonAObjetoLibro(libroEnJson).get(0);
            gestionaResultado(libroModeloActualizado.getTitulo(), libroTitulo);
        } catch (IOException e) {
            e.printStackTrace();
        }




    }

    private ArrayList<String> obtenerListaDeImagenesEnBase64() {
        ArrayList<String> imagenes = new ArrayList<>();

        libro_TilePane.getChildren().forEach(imagenNode -> {
            if (imagenNode instanceof ImageView) {
                String imagenBase64 = obtenerImagenEnBase64((ImageView) imagenNode);
                imagenes.add(imagenBase64);
            }
        });
        return imagenes;
    }


    private void gestionaResultado(String resultado, String titulo) {
        if (resultado.equals(titulo))
            muestraMensajeOK();

        else
            gestionaExcepcion("No se ha podido añadir el libro", new ScrollPane(new TextArea(resultado)));
    }

    private void muestraMensajeOK() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Información");
        alert.setHeaderText(null);
        alert.setContentText("El libro se ha añadido con éxito a la Base de datos");
        alert.showAndWait();
    }

    private boolean tituloEsValido() {

      /*  if (Gestor.existeLibro(libro_txtTitulo.getText().trim())) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("El libro ya existe en la Base de datos");
            alert.showAndWait();
            return false;

        }*/

        if (libro_txtTitulo.getText().trim().isEmpty()) {
            gestionaTituloInvalido();
            return false;
        }

        return true;
    }

    private boolean numPaginasEsValido() {

        if (!libro_txtNumPaginas.getText().isEmpty()) {

            libro_txtNumPaginas.validate();
            if (libro_numberValidator.getHasErrors())
                return false;
        }
        return true;
    }

    private void gestionaTituloInvalido() {
        libro_requiredFieldValidator.setMessage("El Título no puede estar en blanco");
        libro_txtTitulo.validate();
    }

    @FXML
    private void libro_btnSalirOnAction() {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Información");
        alert.setHeaderText("¿Seguro desea salir?");

        ButtonType buttonTypeOne = new ButtonType("Si");
        ButtonType buttonTypeTwo = new ButtonType("No", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);

        Optional<ButtonType> result = alert.showAndWait();
        result.ifPresent(buttonType -> {
            if (buttonType == buttonTypeOne) {
                Stage stage = (Stage) libro_btnSalir.getScene().getWindow();
                stage.close();
            }
        });

    }


}
