package Controlador;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.pame.libros.utils.Gestor;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import utils.modelos.AutorModelo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static utils.PantallasEmergentes.mensajeMuestra;
import static utils.PantallasEmergentes.muestraExcepcion;
import static utils.Utils.transformaDeJsonAObjetoAutor;

public class SelectorAutores {

    public static List<AutorModelo> elementosSeleccionados = new ArrayList<>();
    @FXML
    private JFXTextField selectorAutor_txtAutorNombreFiltrar, selectorAutor_txtAutorApellidoFiltrar;
    @FXML
    private TableView<AutorModelo> selectorAutor_tableView;
    @FXML
    private TableColumn<AutorModelo, String> selectorAutor_tableColumnNombreAutor, selectorAutor_tableColumnWebAutor;
    @FXML
    private TableColumn<AutorModelo, String> selectorAutor_tableColumnApellidosAutor;
    @FXML
    private TableColumn<AutorModelo, CheckBox> selectorAutor_tableColumnCheckBoxes;
    @FXML
    private JFXButton selectorAutor_btnAceptar, selectorAutor_btnSalir;
    @FXML
    private JFXTextField selectorAutor_txtAddAutorNombre, selectorAutor_txtAddAutorApellido, selectorAutor_txtAddAutorWeb;
    @FXML
    private Label selectorAutor_labelFiltrar;
    @FXML
    private VBox selectorAutor_vBox;
    private final ObservableList<AutorModelo> masterData = FXCollections.observableArrayList();
    private final ObjectProperty<Predicate<AutorModelo>> nombreFilter = new SimpleObjectProperty<>();
    private final ObjectProperty<Predicate<AutorModelo>> apellidoFilter = new SimpleObjectProperty<>();

    @FXML
    private void initialize() {

        obtenerAutores();
        creaCheckBoxesColumn();
        creaFiltrosPorNombreYApellido();
    }

    private void creaFiltrosPorNombreYApellido() {
        selectorAutor_tableColumnNombreAutor.setCellValueFactory(cellData -> cellData.getValue().nombreProperty());
        selectorAutor_tableColumnApellidosAutor.setCellValueFactory(cellData -> cellData.getValue().apellidosProperty());
        selectorAutor_tableColumnWebAutor.setCellValueFactory(cellData -> cellData.getValue().webProperty());

        nombreFilter.bind(Bindings.createObjectBinding(() ->
                        autor -> autor.getNombre().toLowerCase().contains(selectorAutor_txtAutorNombreFiltrar.getText().toLowerCase()),
                selectorAutor_txtAutorNombreFiltrar.textProperty()));

        apellidoFilter.bind(Bindings.createObjectBinding(() ->
                        autor -> autor.getApellidos().toLowerCase().contains(selectorAutor_txtAutorApellidoFiltrar.getText().toLowerCase()),
                selectorAutor_txtAutorApellidoFiltrar.textProperty()));

        refreshFilteredList();
    }

    private void creaCheckBoxesColumn() {
        selectorAutor_tableColumnCheckBoxes.setCellValueFactory(arg0 -> {
            AutorModelo user = arg0.getValue();

            CheckBox checkBox = new CheckBox();
            checkBox.selectedProperty().setValue(user.isSeleccionado());
            checkBox.selectedProperty().addListener((ov, old_val, new_val) -> user.setSeleccionado(new_val));
            return new SimpleObjectProperty<>(checkBox);
        });
    }

    private void obtenerAutores() {

        ArrayList<String> autores = Gestor.obtenerTodosLosAutoresEnJson();
        ArrayList<AutorModelo> t;
        try {
            t = transformaDeJsonAObjetoAutor(autores);
            masterData.addAll(t);
        } catch (IOException e) {
            muestraExcepcion("Error al obtener los Autores de la Base de datos", e.getMessage());
            e.printStackTrace();
        }

    }

    @FXML
    private void selectorAutor_btnAddAutorOnAction() {

        String nombreAutor = selectorAutor_txtAddAutorNombre.getText();
        String apellidoAutor = selectorAutor_txtAddAutorApellido.getText();
        String webAutor = selectorAutor_txtAddAutorWeb.getText();

        try {
            String autorEnJson = Gestor.aniadeAutor(nombreAutor, apellidoAutor, webAutor);
            masterData.add(transformaDeJsonAObjetoAutor(autorEnJson));
            refreshFilteredList();
            mensajeMuestra("Autor añadido con éxito a la Base de datos", Alert.AlertType.INFORMATION);
            selectorAutor_txtAddAutorNombre.setText("");
            selectorAutor_txtAddAutorApellido.setText("");
            selectorAutor_txtAddAutorWeb.setText("");

        } catch (Exception e) {
            muestraExcepcion("Error al intentar añadir la autor a la Base de datos", e.getMessage());

        }
    }

    private void refreshFilteredList() {
        FilteredList<AutorModelo> filteredItems = new FilteredList<>(FXCollections.observableList(masterData));

        selectorAutor_tableView.setItems(filteredItems);

        filteredItems.predicateProperty().bind(Bindings.createObjectBinding(
                () -> nombreFilter.get().and(apellidoFilter.get()),
                nombreFilter, apellidoFilter));
    }

    @FXML
    private void selectorAutor_btnAceptarOnAction() {
        elementosSeleccionados = masterData.stream().filter(AutorModelo::isSeleccionado).collect(Collectors.toList());
        Stage stage = (Stage) selectorAutor_btnAceptar.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void selectorAutor_btnSalirOnAction() {
        elementosSeleccionados.clear();
        Stage stage = (Stage) selectorAutor_btnSalir.getScene().getWindow();
        stage.close();
    }

}

