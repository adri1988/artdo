package utils.modelos;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class EditorialModelo {

    private Integer idEditorial;

    public EditorialModelo(Integer idEditorial, String nombreEditorial) {
        this.idEditorial = idEditorial;
        this.seleccionado = new SimpleBooleanProperty(false);
        this.nombreEditorial.setValue(nombreEditorial);
    }

    private final SimpleBooleanProperty seleccionado;
    private final StringProperty nombreEditorial = new SimpleStringProperty();

    public String getNombreEditorial() {
        return nombreEditorial.get();
    }

    public StringProperty nombreEditorialProperty() {
        return nombreEditorial;
    }

    public boolean isSeleccionado() {
        return seleccionado.get();
    }

    public void setSeleccionado(Boolean seleccionado) {
        this.seleccionado.set(seleccionado);
    }

    public Integer getIdEditorial() {
        return idEditorial;
    }

    public void setIdEditorial(Integer idEditorial) {
        this.idEditorial = idEditorial;
    }

    public SimpleBooleanProperty seleccionadoProperty() {
        return seleccionado;
    }

    public void setSeleccionado(boolean seleccionado) {
        this.seleccionado.set(seleccionado);
    }

    public void setNombreEditorial(String nombreEditorial) {
        this.nombreEditorial.set(nombreEditorial);
    }

    @Override
    public String toString() {
        return "EditorialModelo{" +
                "idEditorial=" + idEditorial +
                ", seleccionado=" + seleccionado +
                ", nombreEditorial=" + nombreEditorial +
                '}';
    }
}
