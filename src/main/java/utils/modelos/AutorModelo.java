package utils.modelos;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AutorModelo {
    private final StringProperty nombre = new SimpleStringProperty();
    private final StringProperty apellidos = new SimpleStringProperty();
    private final StringProperty web = new SimpleStringProperty();
    private final SimpleBooleanProperty seleccionado = new SimpleBooleanProperty();
    private final Integer id;


    public AutorModelo(Integer id, String nombre, String apellidos, String web) {
        this.id = id;
        this.nombre.setValue(nombre);
        this.apellidos.setValue(apellidos);
        this.web.setValue(web);
    }


    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public void setApellidos(String apellidos) {
        this.apellidos.set(apellidos);
    }

    public String getWeb() {
        return web.get();
    }

    public void setWeb(String web) {
        this.web.set(web);
    }

    public SimpleBooleanProperty seleccionadoProperty() {
        return seleccionado;
    }

    @Override
    public String toString() {
        return "AutorModelo{" +
                "nombre=" + nombre +
                ", apellidos=" + apellidos +
                ", web=" + web +
                ", seleccionado=" + seleccionado +
                ", id=" + id +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public boolean isSeleccionado() {
        return seleccionado.get();
    }

    public void setSeleccionado(boolean seleccionado) {
        this.seleccionado.set(seleccionado);
    }

    public String getNombre() {
        return nombre.get();
    }

    public StringProperty nombreProperty() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos.get();
    }

    public StringProperty apellidosProperty() {
        return apellidos;
    }

    public StringProperty webProperty() {
        return web;
    }
}
