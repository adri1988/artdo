package utils.modelos;

import Controlador.LibroFormulario;
import Controlador.Principal;
import Controlador.SelectorLibros;
import com.jfoenix.controls.JFXButton;
import com.pame.libros.utils.Gestor;
import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class LibroModelo {


    private Integer idLibro;

    private SimpleStringProperty titulo = new SimpleStringProperty();
    private SimpleStringProperty editorial = new SimpleStringProperty();
    private SimpleStringProperty tematica = new SimpleStringProperty();
    private SimpleStringProperty anioPublicacion = new SimpleStringProperty();
    private SimpleStringProperty soporte = new SimpleStringProperty();
    private SimpleStringProperty idioma = new SimpleStringProperty();
    private String imagen;
    private SimpleStringProperty ISBN = new SimpleStringProperty();
    private SimpleStringProperty autores = new SimpleStringProperty();
    private SimpleBooleanProperty seleccionado = new SimpleBooleanProperty();
    private ImageView imagenLibro;
    private HBox hBox;


    public LibroModelo(Integer idLibro, String titulo, String editorial, String tematica, Integer fecha, String soporte, String idioma, String ISBN, String autores, String imagen) {
        this.idLibro = idLibro;
        this.titulo.setValue(titulo);
        this.editorial.setValue(editorial);
        this.tematica.setValue(tematica);

        if (fecha != null)
            this.anioPublicacion.setValue(fecha.toString());
        this.soporte.setValue(soporte);
        this.idioma.setValue(idioma);
        this.ISBN.setValue(ISBN);
        this.autores.setValue(autores);
        this.imagen = imagen;
    }

    @Override
    public String toString() {
        return "LibroModelo{" +
                "idLibro=" + idLibro +
                ", titulo=" + titulo +
                ", editorial=" + editorial +
                ", tematica=" + tematica +
                ", anioPublicacion=" + anioPublicacion +
                ", soporte=" + soporte +
                ", idioma=" + idioma +
                ", ISBN=" + ISBN +
                ", autores=" + autores +
                '}';
    }

    public boolean isSeleccionado() {
        return seleccionado.get();
    }

    public void setSeleccionado(boolean seleccionado) {
        this.seleccionado.set(seleccionado);
    }

    public SimpleStringProperty autoresProperty() {
        return autores;
    }

    public String getTitulo() {
        return titulo.get();
    }

    public SimpleStringProperty tituloProperty() {
        return titulo;
    }

    public String getEditorial() {
        return editorial.get();
    }

    public SimpleStringProperty editorialProperty() {
        return editorial;
    }

    public SimpleStringProperty tematicaProperty() {
        return tematica;
    }

    public SimpleStringProperty anioPublicacionProperty() {
        return anioPublicacion;
    }

    public SimpleStringProperty soporteProperty() {
        return soporte;
    }

    public SimpleStringProperty idiomaProperty() {
        return idioma;
    }

    public Integer getIdLibro() {
        return idLibro;
    }

    public SimpleStringProperty ISBNProperty() {
        return ISBN;
    }

    public ObservableValue<ImageView> getImageView() {

        try {
            if (!imagen.isEmpty()) {
                imagenLibro = utils.Utils.obtenerImagen(imagen);

                return new ObservableValue<ImageView>() {
                    @Override
                    public void addListener(ChangeListener<? super ImageView> listener) {

                    }

                    @Override
                    public void removeListener(ChangeListener<? super ImageView> listener) {

                    }

                    @Override
                    public ImageView getValue() {
                        return imagenLibro;
                    }

                    @Override
                    public void addListener(InvalidationListener listener) {

                    }

                    @Override
                    public void removeListener(InvalidationListener listener) {

                    }
                };
            }



        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;


    }


    public ObservableValue<HBox> botoneraModifyDelete(SelectorLibros selectorLibros) {

        JFXButton modificar = new JFXButton("Modificar");
        JFXButton eliminar = new JFXButton("Eliminar");
        eliminar.setStyle("-fx-background-color: red; -fx-text-fill: white;");
        modificar.setStyle("-fx-background-color: darkgreen; -fx-text-fill: white;");

        modificar.setOnAction(event -> ejecutaEventoDeModificarSobreElLibro(idLibro, selectorLibros));




        Separator separator = new Separator();

        hBox = new HBox(modificar, separator, eliminar);

        return new ObservableValue<HBox>() {
            @Override
            public void addListener(ChangeListener<? super HBox> listener) {

            }

            @Override
            public void removeListener(ChangeListener<? super HBox> listener) {

            }

            @Override
            public HBox getValue() {
                return hBox;
            }

            @Override
            public void addListener(InvalidationListener listener) {

            }

            @Override
            public void removeListener(InvalidationListener listener) {

            }
        };


    }

    private void ejecutaEventoDeModificarSobreElLibro(Integer idLibro, SelectorLibros selectorLibros) {


        String libro = Gestor.obtenerLibroEnJson(idLibro);
        LibroFormulario.setInitDatos(libro);
        Parent rootLibro = null;
        try {
            rootLibro = FXMLLoader.load(getClass().getResource("/Pantallas/Libro.fxml"));
            Stage stageDeObra = new Stage();
            stageDeObra.getIcons().add(new Image(Principal.class.getResourceAsStream("/Imagenes/LogoApp.png")));
            stageDeObra.setTitle("ArtDo!");
            Scene scene = new Scene(rootLibro);
            stageDeObra.setScene(scene);
            stageDeObra.initModality(Modality.APPLICATION_MODAL);
            stageDeObra.initOwner(hBox.getScene().getWindow());
            stageDeObra.showAndWait();
            this.actualizaDatos(LibroFormulario.getLibroModeloActualizado());
            selectorLibros.initialize();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void actualizaDatos(LibroModelo libroModeloActualizado) {
        this.idLibro = libroModeloActualizado.idLibro;
        this.soporte = libroModeloActualizado.soporte;
        this.tematica = libroModeloActualizado.tematica;
        this.anioPublicacion = libroModeloActualizado.anioPublicacion;
        this.ISBN = libroModeloActualizado.ISBN;
        this.idioma = libroModeloActualizado.idioma;
        this.titulo = libroModeloActualizado.titulo;
        this.autores = libroModeloActualizado.autores;
        this.editorial = libroModeloActualizado.editorial;
        this.imagen = libroModeloActualizado.imagen;


    }
}
