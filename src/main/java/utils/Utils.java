package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jfoenix.controls.JFXComboBox;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import utils.modelos.AutorModelo;
import utils.modelos.EditorialModelo;
import utils.modelos.LibroModelo;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class Utils {

    private static String devuelveAutor(Map<String, Object> autor) {


        return autor.get("nombre") + " " + autor.get("apellidos") + "  \n";

    }

    public static String obtenerImagenEnBase64(ImageView imagenNode) {
        BufferedImage backImg = SwingFXUtils.fromFXImage(imagenNode.getImage(), null);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(backImg, "png", baos);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] imageInByte = baos.toByteArray();
        return Base64.getEncoder().encodeToString(imageInByte);
    }


    public static ArrayList<LibroModelo> transformaDeJsonAObjetoLibro(ArrayList<String> librosEnJson) throws IOException {

        ArrayList<LibroModelo> resultado = new ArrayList<>();

        for (String stringJson : librosEnJson) {
            aniadeAResultadoElJson(resultado, stringJson);
        }
        return resultado;
    }


    private static void aniadeAResultadoElJson(ArrayList<LibroModelo> resultado, String stringJson) throws IOException {
        Map<String, Object> myMap = obtenerMapaDeString(stringJson);

        ArrayList<Map<String, Object>> autores = (ArrayList<Map<String, Object>>) myMap.get("autores");
        ArrayList<Map<String, Object>> editoriales = (ArrayList<Map<String, Object>>) myMap.get("editoriales");
        ArrayList<Map<String, Object>> imagenes = (ArrayList<Map<String, Object>>) myMap.get("imagenes");
        String autoresAMostrar = getAutores(autores);
        String editorialesAMostrar = getEditoriales(editoriales);
        String imagenesAMostrar = devuelvePrimeraImagen(imagenes);
        aniadeDatosAListaResultado(resultado, myMap, autoresAMostrar, editorialesAMostrar, imagenesAMostrar);
    }

    public static Map<String, Object> obtenerMapaDeString(String stringJson) throws IOException {
        Map<String, Object> myMap = new HashMap<String, Object>();
        ObjectMapper objectMapper = new ObjectMapper();
        myMap = objectMapper.readValue(stringJson, HashMap.class);
        return myMap;
    }

    private static String getEditoriales(ArrayList<Map<String, Object>> editoriales) {
        return editoriales.stream().map(Utils::devuelveEditorial)
                .filter(s -> !s.isEmpty())
                .reduce("", String::concat);
    }

    private static String getAutores(ArrayList<Map<String, Object>> autores) {
        return autores.stream().map(Utils::devuelveAutor)
                .filter(s -> !s.isEmpty())
                .reduce("", String::concat);
    }

    private static void aniadeDatosAListaResultado(ArrayList<LibroModelo> resultado, Map<String, Object> myMap, String autoresAMostrar, String editorialesAMostrar, String imagenesAMostrar) {
        resultado.add(new LibroModelo(
                (Integer) myMap.get("idLibros"),
                (String) myMap.get("titulo"),
                editorialesAMostrar,
                (String) myMap.get("tematica"),
                (Integer) myMap.get("anio_de_publicacion"),
                (String) myMap.get("soporte"),
                (String) myMap.get("idioma"),
                (String) myMap.get("isbn"),
                autoresAMostrar,
                imagenesAMostrar)
        );
    }

    private static String devuelvePrimeraImagen(ArrayList<Map<String, Object>> imagenes) {

        if (!imagenes.isEmpty())
            return (String) imagenes.get(0).get("imagen_libro");
        return "";

    }

    private static String devuelveEditorial(Map<String, Object> stringObjectMap) {

        return stringObjectMap.get("nombreEditorial") + " \n";

    }

    public static ImageView obtenerImagen(String imagen) throws IOException {

        ImageView imagenLibro = new ImageView();

        byte[] imageByte;
        imageByte = Base64.getDecoder().decode(imagen);
        BufferedImage image = null;
        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
        image = ImageIO.read(bis);
        bis.close();

        // write the image to a file
        File outputfile = File.createTempFile("Java9", ".png");
        ImageIO.write(image, "png", outputfile);

        Image imageAMostrar = new Image(outputfile.toURI().toURL().toString(), 50, 50, true, true);
        imagenLibro.setImage(imageAMostrar);


        return imagenLibro;
    }

    public static ArrayList<EditorialModelo> transformaDeJsonAObjetoEditorial(ArrayList<String> EditorialesEnJson) throws IOException {

        ArrayList<EditorialModelo> resultado = new ArrayList<>();

        for (String stringJson : EditorialesEnJson) {
            Map<String, Object> myMap = obtenerMapaDeString(stringJson);
            resultado.add(new EditorialModelo((Integer) myMap.get("idEditorial"), (String) myMap.get("nombreEditorial")));


        }
        return resultado;
    }

    public static EditorialModelo transformaDeJsonAObjetoEditorial(String EditorialeEnJson) throws IOException {

        EditorialModelo resultado;


        Map<String, Object> myMap = obtenerMapaDeString(EditorialeEnJson);
        resultado = new EditorialModelo((Integer) myMap.get("idEditorial"), (String) myMap.get("nombreEditorial"));


        return resultado;
    }

    public static AutorModelo transformaDeJsonAObjetoAutor(String autorEnJson) throws IOException {

        AutorModelo resultado;

        Map<String, Object> myMap = obtenerMapaDeString(autorEnJson);
        resultado = new AutorModelo((Integer) myMap.get("idAutor"), (String) myMap.get("nombre"), (String) myMap.get("apellidos"), (String) myMap.get("web"));

        return resultado;
    }

    public static ArrayList<AutorModelo> transformaDeJsonAObjetoAutor(ArrayList<String> autoresEnJson) throws IOException{

        ArrayList<AutorModelo> resultado = new ArrayList<>();

        for (String stringJson: autoresEnJson){
            Map<String, Object> myMap = obtenerMapaDeString(stringJson);
            resultado.add(new AutorModelo((Integer) myMap.get("idAutor"), (String) myMap.get("nombre"), (String) myMap.get("apellidos"), (String) myMap.get("web")));
        }
        return resultado;
    }

    public <T> ArrayList<String> TransformaAJson(ArrayList<T> lista) {

        ObjectMapper mapper = new ObjectMapper();

        ArrayList<String> jsons = new ArrayList<String>();

        for (T elemento : lista) {

            try {
                jsons.add(mapper.writeValueAsString(elemento));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return jsons;
    }

    public static void populaComboBoxSoporte(JFXComboBox<String> comboSoporte) {

        comboSoporte.getItems().add("Tapa Blanda");
        comboSoporte.getItems().add("Tapa Dura");
        comboSoporte.getItems().add("Electrónico");

    }
}
