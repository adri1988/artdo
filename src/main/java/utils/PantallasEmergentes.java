package utils;

import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;

public class PantallasEmergentes {

    public static void mensajeMuestra(String mensaje, Alert.AlertType tipo) {
        Alert alert = new Alert(tipo);
        alert.setHeaderText(mensaje);
        alert.showAndWait();
    }

    public static void muestraExcepcion(String cabecera, String mensajeError) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(cabecera);
        alert.getDialogPane().setExpandableContent(new ScrollPane(new TextArea(mensajeError)));
        alert.showAndWait();
    }
}
