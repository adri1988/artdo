package utils.tematicaUtils;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.Arrays;

@JacksonXmlRootElement(localName = "raiz")
public class Raiz {

    @JacksonXmlProperty(localName = "nodo", namespace = "nodo")
    @JacksonXmlElementWrapper(useWrapping = false)
    private Nodo[] nodo;

    public Raiz() {
    }

    @Override
    public String toString() {
        return "Raiz{" +
                "nodo=" + Arrays.toString(nodo) +
                '}';
    }

    public Nodo[] getNodo() {
        return nodo;
    }

}
