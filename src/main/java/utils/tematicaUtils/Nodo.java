package utils.tematicaUtils;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.Arrays;

public class Nodo {

    @JacksonXmlElementWrapper(localName = "hoja", useWrapping = false, namespace = "hoja")
    private
    Hoja[] hoja;
    @JacksonXmlProperty(isAttribute = true, localName = "nombre")
    private String nombre;
    @JacksonXmlElementWrapper(localName = "nodo", useWrapping = false, namespace = "nodo")
    private Nodo[] nodo;


    public Nodo[] getNodo() {
        return nodo;
    }


    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return "Nodo{" +
                "nombre='" + nombre + '\'' +
                ", hojas=" + Arrays.toString(hoja) +
                '}';
    }

    public Hoja[] getHoja() {
        return hoja;
    }


}
