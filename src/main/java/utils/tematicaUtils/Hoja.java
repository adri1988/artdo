package utils.tematicaUtils;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Hoja {

    @JacksonXmlProperty(isAttribute = true, localName = "id")
    private String id;

    @JacksonXmlProperty(isAttribute = true, localName = "nombre")
    private String nombre;

    @Override
    public String toString() {
        return "Hoja{" +
                "id='" + id + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }

    public String getNombre() {
        return nombre;
    }


}
